class AddFileSizesToJobs < Debci::DB::LEGACY_MIGRATION
  def change
    add_column :jobs, :log_size, :integer
    add_column :jobs, :artifacts_size, :integer
  end
end
